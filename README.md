This Chrome/Chromium extension allows you to mass-download images from common one-click hosters (like ImageVenue, imgur, ImageBam, etc.) usually used on various image boards and websites.

This extension comes fully localized in German and English and does not need any 3rd-party applications installed on the computer. All you need is your web browser and this extension!


Usage
-----

You might want to set up Chrome so it does not ask where to store the downloads but automatically downloads them without bringing up the save-as dialog.

1. Open the tabs you want to scrape
2. Run the extension
3. Select the tabs from the tabs list and click “Scrape tabs”
4. Select the links of the images you want to download, or select all of them
5. When set: Enter the download path relative from you default download folder
6. Click “Download images” and wait until all images are downloaded
7. Close the window or start over


Options
-------

The options allow you to set the pre-defined download path. It’s either none, a path containing date and time, a folder name you enter, or the options to always show the path input field.


Currently supported hosters
---------------------------

* ImageBam (imagebam.com)
* imgbox (imgbox.com)
* ImageVenue (imagevenue.com)
* imgur (imgur.com)
* TurboImageHost (turboimagehost.com)
* Facebook (facebook.com)
* flickr (flickr.com)
* Issuu PDF Downloader (issuu-downloader.abuouday.com)
* Tumblr (tumblr.com)
* Instagram (instagram.com)
* Hotflick Image Upload (hotflick.net)
* SomeImage (someimage.com)
* Reddit uploads (reddituploads.com)
* Pixhost (pixhost.org)
* ImgCredit.xyz
* ImageTwist


Add/Suggest hosters
-------------------

Adding hosters needs some Javascript. A technical documentation on how to do this is available in `downloader/hosters.js`.

If you don’t know Javascript you can simply suggest a hoster via e-mail or message, or as issue on GitLab (see links).


A word on permissions
---------------------

This extension uses some permissionsthat might be seen as critical by some users. Besides the fact that interested users can check the extension’s code at GitLab, the following permissions are needed.

1. **tabs** for extracting the page’s title and collecting the tab ID for injecting the necessary Javascript code for image collection.
2. **downloads** to initiate the image downloads.
3. **storage** for reading and writing the extension’s options.
4. **all_urls** because the extension needs “write permission” on all pages for injecting the necessary Javascript code for image collection.

Feel free to analyze the code at GitLab.


A word on the version number
----------------------------

The version number is actually not only a version number. It is a version string which contains two informations. The first quarter is the version number without dots (1.2.3 becomes 123). The second to fourth quarter represents the date of when the last hoster was added to the “database”. It written in ISO 8601 format (YYYY-MM-DD) without leading zeros and dashes replaced by dots (2015-05-21 becomes 2015.5.21).

The Chrome Web Store uses the version string and displays it correctly (Version number, space, date).


Bugs
----

Sometimes the images get not recognized the first time. I set a short waiting period for chrome inserting the necessary Javascript code to the website displayed in the selected tab(s), but sometimes it takes too long. Simply restart the process and the images will be there.


Changelog
---------

A changelog with all commits made to this extension is available ad GitLab where it’s been actively developed.

https://gitlab.com/4w/oneclick-downloader/commits/master
