function scanTabs() {
    chrome.tabs.query({}, function(tabs) {

        var res = '';
        var behavior = getStartupBehavior();

        for (var i=0; i<tabs.length; i++) {
            if (tabs[i]['title'] == document.title)
                continue;

            if (tabs[i]['url'].substring(0, 6) == 'chrome')
                continue;

            var id = tabs[i]['id'];
            var name = tabs[i]['title'];
            var cssclass = 'inactive';
            var options = 'type="checkbox" class="tabInfo"';

            if ((behavior === 'current') || (behavior === 'links')) {
                if (tabs[i]['active'] === true) {
                    options += ' checked="checked"';
                    cssclass = 'active';
                }
            }

            res += '<label class="'+cssclass+'">';
            res += '<input '+options+' value="'+id+'" />';
            res += '<span class="tabTitle">'+name+'</span>';
            res += '</label>';
        }

        document.getElementById('tabsList').innerHTML = res;

        if (behavior === 'links') {
            scrape();
        }

    });
}


function scrape() {
        downloadPath();
        document.getElementById('tabs').style.display = 'none';
        document.getElementById('scrape').style.display = 'block';
        document.getElementById('appHeader').innerHTML = t('linksHeader', true);
        setTimeout(function() {getLinks()}, 500);
}


document.addEventListener('DOMContentLoaded', function() {
    setStartupBehavior();
    document.title = t('appName', true);
    scanTabs();
    document.getElementById('appHeader').innerHTML = t('tabsHeader', true);
    document.getElementById('scrape').style.display = 'none';
    document.getElementById('download').style.display = 'none';

    tabsRescan = document.getElementById('tabsRescan');
    tabsRescan.addEventListener('click', scanTabs);

    tabsCheckAll = document.getElementById('tabsCheckAll');
    tabsCheckAll.addEventListener('click', function () {
        var cbs = document.getElementsByClassName('tabInfo');
        for(var i=0; i < cbs.length; i++) {
            if(cbs[i].type == 'checkbox') {
                cbs[i].checked = true;
            }
        }
    });

    tabsUnheckAll = document.getElementById('tabsUncheckAll');
    tabsUncheckAll.addEventListener('click', function () {
        var cbs = document.getElementsByClassName('tabInfo');
        for(var i=0; i < cbs.length; i++) {
            if(cbs[i].type == 'checkbox') {
                cbs[i].checked = false;
            }
        }
    });

    tabsScrape = document.getElementById('tabsScrape');
    tabsScrape.addEventListener('click', scrape);


});



