chrome.runtime.onMessage.addListener(function(request, sender, sendResponse) {

    if (request.action == 'getLinks') {

        var links = document.getElementsByTagName('a');
        var hosters = request.hosters;
        var regexArray = [];
        var res = [];

        for (var i=0; i < hosters.length; i++) {
            regexArray.push(hosters[i]['pattern']);
        }

        for (var i=0; i < links.length; i++) {
            var href = links[i].href;
            for (var j=0; j < regexArray.length; j++) {
                var regex = new RegExp(regexArray[j], 'i');
                if (regex.test(href) === true) {
                    res.push(href);
                }
            }
        }

        res = res.filter(function(value, index, self) {
            return self.indexOf(value) === index;
        });

        sendResponse({links: res});

    }

});
