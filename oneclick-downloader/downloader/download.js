function downloadImages () {

    log = document.getElementById('downloadLog');

    var links = document.getElementsByClassName('linkInfo');
    var wantedLinks = [];

    for (var i=0; i < links.length; i++) {
        if(links[i].type !== 'checkbox')
            continue;

        if (links[i].checked !== true)
            continue;

        getPage(links[i].value);
    }
}


function getPage(url) {
    var xhr = new XMLHttpRequest();
    xhr.open('GET', url, true);
    xhr.responseType = 'document';
    xhr.onload = function(e) {
        downloadImage(this.response, url.getHost(), url);
    };

    xhr.send();
}


function downloadImage(c, hostname, url) {
    var imageData = false;
    var counter = 0;

    for (var i=0; i < supportedHosters.length; i++) {
        var hoster = supportedHosters[i];
        var regex = new RegExp(hoster['pattern'], 'i');
        if (regex.test(url) === true) {
            // … and GO!
            imageData = window[hoster['function']](c, url);
            break;
        }
    }

    if (imageData === false)
        return;

    var path = document.getElementById('downloadPath').value;
    var filename = imageData['name'];

    path = (path.length === 0) ? '' : path + '/';
    filename = filename.replace(/[\/:"?~<>*|]/g, '-');

    chrome.downloads.download({
        'url': imageData['url'],
        'filename': path + filename,
        'saveAs': false // gets ignored for unknown reason
    }, function (object) {});

}


document.addEventListener('DOMContentLoaded', function() {

    downloadClose = document.getElementById('downloadClose');
    downloadClose.addEventListener('click', function() {
        window.close();
    });

    downloadRestart = document.getElementById('downloadRestart');
    downloadRestart.addEventListener('click', function() {
        location.reload();
    });


});

chrome.downloads.onCreated.addListener(function (object) {
    var ol = document.getElementById('downloadLog');
    var li = document.createElement('li');
    var a = document.createElement('a');
    var text = '[' + object['url'].getHost() + '] ' + object['url'].getLast();
    a.appendChild(document.createTextNode(text));
    a.href = object['url'];
    li.id = 'download-'+object['id'];
    li.className = object['state'];
    li.appendChild(a);
    ol.appendChild(li);
});


chrome.downloads.onChanged.addListener(function (object) {
    if (object['state'] !== undefined) {
        var entry = document.getElementById('download-'+object['id']);
        entry.className = object['state']['current'];
    }
});
