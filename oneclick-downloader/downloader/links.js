function getLinks() {
    var tabs = document.getElementsByClassName('tabInfo');
    var wantedTabs = [];
    var res = '';


    for (var i=0; i < tabs.length; i++) {
        if(tabs[i].type !== 'checkbox')
            continue;

        if (tabs[i].checked !== true)
            continue;

        var id = parseInt(tabs[i].value);

        chrome.tabs.executeScript(id, {'file': 'downloader/injection.js'});

        var messageObject = {
            'action': 'getLinks',
            'hosters': supportedHosters
        };

        // sleep() simulator for possible race condition of the above
        // chrome.tabs.executeScript() and the below ….senMessage(), this
        // delays the execution for each tab by 500ms.
        var sleep = new Date().getTime();
        while (new Date().getTime() < sleep + 500);

        chrome.tabs.sendMessage(id, messageObject, function(result, res) {
            if (result.links !== undefined) {
                var links = result.links;
            } else {
                var links = [];
            }

            var linksList = document.getElementById('linksList');

            for (var i=0; i < links.length; i++) {
                var link = links[i];
                var l = '<label>'+
                    '<input type="checkbox" value="'+link+'" class="linkInfo">'+
                    '<span>'+link+'</span>'+
                    '</label>';
                linksList.innerHTML += l;
            };

            var loadingLinks = document.getElementById('loadingLinks');
            loadingLinks.style.display = 'none';

        });

    }


    linksCheckAll = document.getElementById('linksCheckAll');
    linksCheckAll.addEventListener('click', function () {
        var cbs = document.getElementsByClassName('linkInfo');
        for(var i=0; i < cbs.length; i++) {
            if(cbs[i].type == 'checkbox') {
                cbs[i].checked = true;
            }
        }
    });

    linksUnheckAll = document.getElementById('linksUncheckAll');
    linksUncheckAll.addEventListener('click', function () {
        var cbs = document.getElementsByClassName('linkInfo');
        for(var i=0; i < cbs.length; i++) {
            if(cbs[i].type == 'checkbox') {
                cbs[i].checked = false;
            }
        }
    });

    downloaderRestart = document.getElementById('downloaderRestart');
    downloaderRestart.addEventListener('click', function () {
        location.reload();
    });

    linksDownload = document.getElementById('linksDownload');
    linksDownload.addEventListener('click', function () {
        document.getElementById('scrape').style.display = 'none';
        document.getElementById('download').style.display = 'block';
        document.getElementById('appHeader').innerHTML =
            t('downloadHeader', true);
        downloadImages()
    });


}
