function downloadPath() {

    chrome.storage.sync.get({
        'downloadPathOption': 'date',
        'downloadCustomPath': ''
    }, function(items) {

        var dlpath = '';
        var dlpathLabel = document.getElementById('downloadPathLabel');

        switch (items.downloadPathOption) {
            case 'custom':
                dlpath = items.downloadCustomPath;
                dlpathLabel.style.display = 'none';
                break;
            case 'date':
                dlpath = setDateTime();
                dlpathLabel.style.display = 'none';
                break;
            case 'none':
                dlpath = null;
                dlpathLabel.style.display = 'none';
                break;
            case 'request':
                dlpath = '';
                break;
        }

        document.getElementById('downloadPath').value = dlpath;

    });

}


function setDateTime() {
    var date = new Date();
    return t('appName', true) + ', '
        + date.getFullYear() + '-'
        + (('0'+(date.getMonth()+1)).slice(-2)) + '-'
        + ('0'+date.getDate()).slice(-2) + ', '
        + ('0'+date.getHours()).slice(-2) + '-'
        + ('0'+date.getMinutes()).slice(-2) + '-'
        + ('0'+date.getSeconds()).slice(-2);

}


// > var s = 'http://www.example.com/foo/bar/baz.jpg';
// > s.getLast();
// = 'baz.jpg'
String.prototype.getLast = function() {
    return this.substr(this.lastIndexOf('/') + 1);
};


// > var h = 'http://www.example.com/foo/bar/baz.jpg'
// > h.getHost();
// = 'example.com'
String.prototype.getHost = function() {
    var parser = document.createElement('a');
    var hostname = '';
    parser.href = this;
    hostname = parser.hostname.replace('www.', '').split('.').reverse();
    return hostname[1]+'.'+hostname[0];
}


// > var h = 'Hello World!'
// > h.startsWith('Hel');
// = true
// > h.startsWith('blah');
// = false
String.prototype.startsWith = function(check) {
    return this.slice(0, check.length) == check;
}


// > var n = 'Image%20%281%29.jpg'
// > n.decode()
// = 'Image (1).jpg'
String.prototype.decode = function() {
    return decodeURI(this);
}
