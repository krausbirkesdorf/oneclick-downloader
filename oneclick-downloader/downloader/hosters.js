// PREAMBLE
//
// You can suggest hosters or provide a complete definition (the object and the
// function) for any hoster you want. Simply file an issue on GitHub or contact
// me via the contact form in Google Chrome Store or simply write me an e-mail
// to spam@0x7be.de to reach me.
//
// Hosters should be actual one click image hosters and similar and no private
// websites or fanpages/galleries. Exceptions can be made if there are any
// benefits in it.
//
//
// THE OBJECT
//
// Each definition has to push an object to supportedHosters[] containing all
// necessary meta information an identifications attributes. The following
// items are mandatory.
//
// hoster:    This value holds the official name of the hosting service and is
//            for informational purposes only. Try to correctly determine and
//            use the official name of the service.
//
// hostname:  The actual host name on which the images are hosted. This has to
//            be the basic host name without any subdomains. For example, if
//            the images are hosted at img.example.com the hostname value has
//            to be example.com without the “img.” part.
//
// pattern:   A Javascript regular expression part (without any modifiers) that
//            matches the links the hoster uses. This should be as simple as
//            possible to prevent breakage or simply not matching any links.
//            For example the hoster uses http://example.com/image.php?id=123
//            as image link the pattern should be only match the part up the ID
//            part: “example\\.com/image\\.php\\?id=”.
//            The pattern testing is case insensitive and is greedy by default
//            so you don’t need to add that in special cases. You need to
//            double-escape special characters like quesion marks or the dot
//            but you don’t need to escape slashes.
//
// function:  The name of the function to be used for matched links. Of course
//            a corresponding function has to be created in order to work.
//
// author:    The name or nickname of the author who provided the definition.
//            This gets shown on the definitions overview page.
//
// contact:   Contact information (e-mail or website URL) of the author. If
//            you don’t want any contact information being shown simply use
//            the null value instead.
//
// added:     Date when the definition was first added or updated after it was
//            initially added.
//
// comment:   A short comment on – for example – where the definition is used
//            or what to keep in mind when using it.
//
// idea:      An optional object holding information about who suggested
//            this hoster via what channel and when.
//            {'name': 'John Doe',
//             'channel': 'GitHub',
//             'date': '2015-11-15'}
//
//
// THE FUNCTION
//
// Functions have access to two values. c and u. c is the content of the
// currently processed image page as DOM structure and u is the URL of the
// image page that is currently processed. If you want to only access u you
// need to add c, too. Just add them as parameters in your function definition
// to use them within the function.
//
//
// PROTOTYPE FUNCTIONS
//
// Functions have access to four string-related prototype functions.
//
// string.getLast() returns the last part of a slash separated value. For
// example an URL or file path.
//
//   > var s = 'http://www.example.com/foo/bar/baz.jpg';
//   > s.getLast();
//   = 'baz.jpg'
//
// string.getHost() returns the hostname of a given URL. This might break on
// multi-part TLDs (co.uk, com.au, etc.)
//
//   > var h = 'http://www.example.com/foo/bar/baz.jpg'
//   > h.getHost();
//   = 'example.com'
//
// string.startsWith() determines wether the given string starts with the
// given string or not.
//
//   > var h = 'Hello World!'
//   > h.startsWith('Hel');
//   = true
//   > h.startsWith('blah');
//   = false
//
// string.decode() decodes the given string. Currently this is practically
// an alias for the built-in javascript function decodeURI() in further
// versions it might check for other stuff as well.
//
//   > var n = 'Image%20%281%29.jpg'
//   > n.decode()
//   = 'Image (1).jpg'
//
// Feel free to check this documentation from time to time to see if there are
// more functions available that can replace some code in your definition and
// then simply re-submit it so I can update it.
//
//
// THE RETURN VALUE
//
// All definitions have to return an object containing the full image URL and
// the actual image file name.
//
// url:   The URL of the image should be the URL of the full-size image that
//        was determined by your function. When downloading the document
//        referred to by the URL the result should be an image file.
//
// name:  Try to extract the actual file name from the provided DOM structure
//        or the image page URL and strip all additional data added by the
//        hoster. For example: id123_theFileName_fullsize.jpg should result in
//        theFileName.jpg
//
// Desist from using a hard-coded filename because there are usually multiple
// images being downloaded and not only one at a time.




supportedHosters = [];




supportedHosters.push({
    'hoster': 'ImageBam',
    'hostname': 'imagebam.com',
    'pattern': 'imagebam\\.com/image/',
    'function': 'imagebam',
    'author': 'Dirk Sohler',
    'contact': 'http://0x7be.de',
    'added': '2015-05-15',
    'comment': 'Very common on celebrity image boards'
});

function imagebam(c) {
    var imgs = c.getElementsByTagName('img');
    for (var i = 0; i < imgs.length; i++) {
        if (imgs[i].alt == 'loading') {
            var url = imgs[i].src;
            var name = imgs[i].src.getLast()
            return { 'url': url, 'name': name.decode() };
        }
    }
}




supportedHosters.push({
    'hoster': 'imgbox',
    'hostname': 'imgbox.com',
    'pattern': '/imgbox\\.com/',
    'function': 'imgbox',
    'author': 'Dirk Sohler',
    'contact': 'http://0x7be.de',
    'added': '2015-05-15',
    'comment': 'Very common on celebrity image boards'
});

function imgbox(c) {
    var img = c.getElementById('img');
    return {
        'url': img.src,
        'name': img.title
    };
}




supportedHosters.push({
    'hoster': 'ImageVenue',
    'hostname': 'imagevenue.com',
    'pattern': '\\.imagevenue\\.com/img\\.php\\?',
    'function': 'imagevenue',
    'author': 'Dirk Sohler',
    'contact': 'http://0x7be.de',
    'added': '2015-05-15',
    'comment': 'Very common on celebrity image boards'
});

function imagevenue(c) {
    var url = c.getElementById('thepic').src;
    var name = url.getLast();
    var pattern = '^[0-9]*_(.*)_[0-9]*_[0-9]*[a-zA-Z]*.([a-zA-Z]*)$';
    var regex = new RegExp(pattern, 'g');
    return {
        'url': url,
        'name': name.replace(regex, '$1.$2')
    };
}




supportedHosters.push({
    'hoster': 'imgur',
    'hostname': 'imgur.com',
    'pattern': '://(i\\.)?imgur\\.com/',
    'function': 'imgur',
    'author': 'Dirk Sohler',
    'contact': 'http://0x7be.de',
    'added': '2015-05-16',
    'comment': 'Widely used on Reddit'
});

function imgur(c, u) {
    var direct = u.includes('/i.imgur.com/');
    var res = {};

    if (direct === true) {
        res['url'] = u;
        res['name'] = u.getLast();
    } else {
        var div = c.getElementsByClassName('post-image')[0];
        var img = div.getElementsByTagName('img')[0].src;
        res['url'] = img;
        res['name'] = img.getLast();
    }

    return res;
}




supportedHosters.push({
    'hoster': 'TurboImageHost',
    'hostname': 'turboimagehost.com',
    'pattern': 'turboimagehost\\.com/',
    'function': 'turboimagehost',
    'author': 'Dirk Sohler',
    'contact': 'http://0x7be.de',
    'added': '2015-06-09',
    'comment': 'Used on some celebrity image boards'
});

function turboimagehost(c) {
    var url = c.getElementById('imageid').src;
    var name = url.getLast();
    return {
        'url': url,
        'name': name
    };
}




supportedHosters.push({
    'hoster': 'Facebook',
    'hostname': 'facebook.com',
    'pattern': 'facebook\\.com/(.*/photos/|photo\\.php\\?fbid)',
    'function': 'facebookGallery',
    'author': 'Dirk Sohler',
    'contact': 'http://0x7be.de',
    'added': '2015-06-15',
    'comment': 'Works best with Facebook galleries'
});

function facebookGallery(c, u) {
    var url = '';
    var name = '';
    var downloadBase = 'https://www.facebook.com/photo/download/?fbid=';
    var photos = /\/photos\/[0-9a-z.-]*\/([0-9]*)/i;
    var fbid = /photo\.php\?fbid\=([0-9]*)/i;

    var photosMatch = u.match(photos);
    var fbidMatch = u.match(fbid);

    if (photosMatch !== null) {
        url = downloadBase + photosMatch[1];
        name = photosMatch[1] + '.jpg';
    } else if (fbidMatch !== null) {
        url = downloadBase + fbidMatch[1];
        name = fbidMatch[1] + '.jpg';
    }

    return { 'url': url, 'name': name };
}




supportedHosters.push({
    'hoster': 'flickr',
    'hostname': 'flickr.com',
    'pattern': 'flickr\\.com/photos/:?[a-z0-9_\\-\\.@]*/[0-9]*/$',
    'function': 'flickrAlbum',
    'author': 'Dirk Sohler',
    'contact': 'http://0x7be.de',
    'added': '2015-07-03',
    'comment': 'VERY nasty plaintext parsing of JS code and very wonky'
});

function flickrAlbum(c, u) {
    var variants = [];
    var url = '';
    var modelExportText = c.getElementsByClassName('modelExport')[0].innerText;
    var modelExport = modelExportText.replace(/(\r\n|\n|\r)/gm, '').split('"');

    for (var i = 0; i < modelExport.length; i++) {
        if (modelExport[i].indexOf('\\/\\/farm') === 0)
            variants.push(modelExport[i]);
    }

    url = u.split(':')[0] + ':' + variants.pop();

    return {
        'url': url.replace(/\\/g, ''),
        'name': url.getLast()
    };
}




supportedHosters.push({
    'hoster': 'Issuu PDF Downloader',
    'hostname': 'abuouday.com',
    'pattern': 'issuu-downloader\\.abuouday\\.com/single\\.php\\?id=.*',
    'function': 'IssuuPdfDownloader',
    'author': 'Dirk Sohler',
    'contact': 'http://0x7be.de',
    'added': '2015-11-15',
    'comment': 'Online PDF to image converter',
    'idea': { 'name': 'jaredbidlow', 'channel': 'GitHub', 'date': '2015-11-12' }
});

function IssuuPdfDownloader(c) {
    var url = c.getElementById('img').src;
    var name = url.getLast();
    return {
        'url': url,
        'name': name
    };
}




supportedHosters.push({
    'hoster': 'Tumblr',
    'hostname': 'tumblr.com',
    'pattern': 'tumblr\\.com/image/[0-9]*',
    'function': 'tumblrImages',
    'author': 'Dirk Sohler',
    'contact': 'http://0x7be.de',
    'added': '2015-11-21',
    'comment': 'Gets images in 1280 variant and fails with 404 if not available'
});

function tumblrImages(c) {
    var url = c.getElementById('content-image').getAttribute('data-src');
    var name = url.getLast().replace('tumblr_', '').replace('_1280', '');
    return {
        'url': url,
        'name': name
    };
}




supportedHosters.push({
    'hoster': 'Instagram',
    'hostname': 'instagram.com',
    'pattern': 'instagram\\.com/p/.*',
    'function': 'instagramImages',
    'author': 'Dirk Sohler',
    'contact': 'http://0x7be.de',
    'added': '2015-11-21',
    'comment': 'Using og: meta tags which hold the full size image/video'
});

function instagramImages(c) {
    var metatags = c.getElementsByTagName('meta');
    var data = false;

    for (var i = 0; i < metatags.length; i++) {
        if (metatags[i].getAttribute('property') === 'og:image') {
            data = metatags[i].getAttribute('content');
            break;
        }
    }

    for (var i = 0; i < metatags.length; i++) {
        if (metatags[i].getAttribute('property') === 'og:video') {
            data = metatags[i].getAttribute('content');
            break;
        }
    }

    if (!data)
        return;

    return {
        'url': data,
        'name': data.getLast().split('?')[0]
    };
}




supportedHosters.push({
    'hoster': 'Hotflick Image Upload',
    'hostname': 'hotflick.net',
    'pattern': 'hotflick.net/././\\?q=.*',
    'function': 'hotflick',
    'author': 'Dirk Sohler',
    'contact': 'http://0x7be.de',
    'added': '2016-10-29',
    'comment': 'Used on some celebrity image boards',
    'idea': { 'name': 'GitBoudewijn', 'channel': 'GitHub', 'date': '2016-10-29' }
});

function hotflick(c) {
    var url = c.getElementById('img').src;
    var pattern = '^[a-zA-Z]*_[0-9]*_(.*)_[0-9]*_[0-9]*[a-zA-Z]*.([a-zA-Z]*)$';
    var regex = new RegExp(pattern, 'g');
    return {
        'url': url,
        'name': url.getLast().replace(regex, '$1.$2')
    };
}




supportedHosters.push({
    'hoster': 'SomeImage',
    'hostname': 'someimage.com',
    'pattern': '://(www\\.)?someimage.com/[0-9a-zA-Z]*',
    'function': 'someimage',
    'author': 'Dirk Sohler',
    'contact': 'http://0x7be.de',
    'added': '2016-10-29',
    'comment': 'Used on some celebrity image boards',
    'idea': { 'name': 'GitBoudewijn', 'channel': 'GitHub', 'date': '2016-10-29' }
});

function someimage(c) {
    var url = c.getElementById('viewimage').src;
    var name = url.getLast();
    return {
        'url': url,
        'name': name.decode()
    };
}




supportedHosters.push({
    'hoster': 'Reddit uploads',
    'hostname': 'reddituploads.com',
    'pattern': '://i.reddituploads.com/[0-9a-zA-Z]*',
    'function': 'reddituploads',
    'author': 'Dirk Sohler',
    'contact': 'http://0x7be.de',
    'added': '2016-10-29',
    'comment': 'Used on some celebrity image boards',
    'idea': { 'name': 'GitBoudewijn', 'channel': 'GitHub', 'date': '2016-10-29' }
});

function reddituploads(c, u) {
    return {
        'url': u,
        'name': u.getLast().split('?')[0] + '.jpg'
    };
}




supportedHosters.push({
    'hoster': 'ImgCredit.XYZ',
    'hostname': 'wwv.imgcredit.xyz',
    'pattern': 'wwv\\.imgcredit\\.xyz/image/',
    'function': 'imgcredit',
    'author': 'Arhey',
    'contact': 'https://github.com/freearhey',
    'added': '2017-12-24',
    'comment': 'Very common on celebrity image boards'
});

function imgcredit(c) {
    var link = c.getElementsByClassName('btn-download')[0]
    var url = link.href;
    var name = link.download;
    return {
        'url': url,
        'name': name
    };
}




supportedHosters.push({
    'hoster': 'ImageTwist',
    'hostname': 'imagetwist.com',
    'pattern': 'imagetwist\\.com/',
    'function': 'imagetwist',
    'author': 'Arhey',
    'contact': 'https://github.com/freearhey',
    'added': '2018-01-03',
    'comment': 'Very common on celebrity image boards'
});

function imagetwist(c, u) {
    var img = c.getElementsByClassName('pic')[0];
    var url = img.src;
    var name = url.getLast();
    return {
        'url': url,
        'name': name
    };
}




supportedHosters.push({
    'hoster': 'PiXhost',
    'hostname': 'pixhost.to',
    'pattern': 'pixhost\\.to/show/',
    'function': 'pixhost',
    'author': 'Arhey',
    'contact': 'https://github.com/freearhey',
    'added': '2018-03-29',
    'comment': 'Very common on celebrity image boards'
});

function pixhost(c) {
    var img = c.getElementById('image')
    var url = img.src;
    var name = img.alt;
    return {
        'url': url,
        'name': name
    };
}
