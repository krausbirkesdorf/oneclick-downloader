chrome.browserAction.onClicked.addListener(function() {

    var windowWidth = 1024;
    var windowHeight = 576;
    var fromLeft = (screen.width/2)-(windowWidth/2);
    var fromTop = (screen.height/2)-(windowHeight/2);

    chrome.tabs.create({
        url: chrome.extension.getURL('downloader.html'),
        active: false
    }, function(tab) {
        chrome.windows.create({
            'tabId': tab.id,
            'type': 'popup',
            'focused': true,
            'width': windowWidth,
            'height': windowHeight,
            'left': fromLeft,
            'top': fromTop
        });
    });


});
