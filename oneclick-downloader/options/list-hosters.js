document.addEventListener('DOMContentLoaded', function() {

    var infoDiv = document.getElementById('hostersInfo');
    var table = document.createElement('table');
    var thead = document.createElement('thead');
    var tbody = document.createElement('tbody');
    var theadRow = document.createElement('tr');



    // Create head

    var columns = [
        t('hostersTableName', true),
        t('hostersTableHostname', true),
        t('hostersTableAuthor', true),
        t('hostersTableAdded', true)
    ];

    columns.forEach(function(entry){
        var th = document.createElement('th');
        th.appendChild(document.createTextNode(entry));
        theadRow.appendChild(th);
    });



    // Create body

    var supportedHostersSorted = supportedHosters.sort(function(a, b) {
        var x = a['hoster'];
        var y = b['hoster'];
        if (typeof x == "string") {
            x = x.toLowerCase(); 
            y = y.toLowerCase();
        }
        return ((x < y) ? -1 : ((x > y) ? 1 : 0));
    });

    for (var i=0; i < supportedHostersSorted.length; i++) {
        var author;
        var h = supportedHostersSorted[i];
        var tr = document.createElement('tr');
        var tdHoster = document.createElement('td');
        var tdHostname = document.createElement('td');
        var tdAuthor = document.createElement('td');
        var tdAdded = document.createElement('td');

        if (h['contact'] !== null) {
            author = document.createElement('a');
            author.href = h['contact'];
            author.appendChild(document.createTextNode(h['author']));
        } else {
            author = document.createTextNode(h['author']);
        }

        tdHoster.appendChild(document.createTextNode(h['hoster']));
        tdHostname.appendChild(document.createTextNode(h['hostname']));
        tdAuthor.appendChild(author);
        tdAdded.appendChild(document.createTextNode(h['added']));

        tr.appendChild(tdHoster);
        tr.appendChild(tdHostname);
        tr.appendChild(tdAuthor);
        tr.appendChild(tdAdded);

        tbody.appendChild(tr);
    }



    // Build and append table

    thead.appendChild(theadRow);
    table.appendChild(thead);
    table.appendChild(tbody);
    infoDiv.appendChild(table);



    // Back to options button
    backToOptionsButton = document.getElementById('backToOptionsButton');
    backToOptionsButton.addEventListener('click', function() {
        location.href = 'options.html';
    });



});
